rainbow.js (2.1.7+ds-2) unstable; urgency=medium

  * Team upload
  * Update standards version to 4.6.1, no changes needed.
  * Fix debian/watch

 -- Yadd <yadd@debian.org>  Mon, 31 Oct 2022 12:02:02 +0100

rainbow.js (2.1.7+ds-1) unstable; urgency=medium

  * Team upload
  * Fix GitHub tags regex
  * Update standards version to 4.6.0, no changes needed.
  * New upstream version 2.1.7+ds
  * Refresh patches
  * Update lintian overrides

 -- Yadd <yadd@debian.org>  Sat, 11 Dec 2021 09:06:40 +0100

rainbow.js (2.1.4+ds-4) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Apply multi-arch hints.
    + libjs-rainbow: Add Multi-Arch: foreign.

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 13
  * Declare compliance with policy 4.5.1
  * Update build dependencies: replace node-uglify by uglifyjs
    (Closes: #979953)

 -- Xavier Guimard <yadd@debian.org>  Tue, 12 Jan 2021 12:51:24 +0100

rainbow.js (2.1.4+ds-3) unstable; urgency=medium

  * Team upload

  [ Pirate Praveen ]
  * Build with rollup 1.10 and add node-yargs as build dep

  [ Xavier Guimard ]
  * Declare compliance with policy 4.5.0
  * Add "Rules-Requires-Root: no"
  * Update lintian overrides
  * Add fix for gulp 4 (Closes: #954682)

 -- Xavier Guimard <yadd@debian.org>  Mon, 23 Mar 2020 13:43:55 +0100

rainbow.js (2.1.4+ds-2) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.4.1
  * Update rollup parameters (Closes: #941604)

 -- Xavier Guimard <yadd@debian.org>  Fri, 11 Oct 2019 19:13:04 +0200

rainbow.js (2.1.4+ds-1) unstable; urgency=medium

  * Team upload
  * Fix debian/watch
  * Bump debhelper compatibility level to 12
  * Declare compliance with policy 4.4.0
  * Change section to javascript
  * Change priority to optional
  * Add debian/gbp.conf
  * Add upstream/metadata
  * Back to +ds
  * New upstream version 2.1.4+ds
  * Reproduce upstream build using gulp
  * Add lintian-overrides file
  * Embed rollup-plugin-uglify (for build only)
  * debian/control: fix VCS fields, add build dependencies and fix description
  * Update install and fix nodejs module install
  * Update debian/copyright
  * Add debian/clean
  * Enable minimal autopkgtest based on pkg-js-autopkgtest

 -- Xavier Guimard <yadd@debian.org>  Tue, 16 Jul 2019 14:36:57 +0200

rainbow.js (1.1.8+ds1-1) unstable; urgency=low

  * Initial release

 -- David Paleino <dapal@debian.org>  Fri, 12 Oct 2012 12:47:18 +0200
